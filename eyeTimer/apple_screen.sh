#!/bin/bash

# Activating the module
modprobe i2c-dev

every=20

while true
do
  for i in {0..$every};
  do
    remaining=$(echo "$every - $i" | bc)
    printf "Will dim in %s minutes...\r" "$remaining"
    sleep 1m
  done
  echo ""

  curr=$(ddcutil getvcp 10 | tr -s ',' ' ' | tr -s ' ' | cut -d\  -f 9)
  # echo "Current brightness: $curr"

  ddcutil setvcp 10 0
  sleep $every
  ddcutil setvcp 10 $curr
done

