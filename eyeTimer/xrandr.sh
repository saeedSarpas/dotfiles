#!/bin/zsh

every=20

while true
do
  for i in {0..$every};
  do
    remaining=$(echo "$every - $i" | bc)
    printf "Will dim in %2s minutes...\r" "$remaining"
    sleep 1m
  done
  echo ""

  curr=($(xrandr --current --verbose | grep -i brightness | sed 's/\s\s*/ /g' | cut -f 3 -d ' '))
  outputs=($(xrandr -q | grep " connected" | cut -f 1 -d ' '))

  for i in {1..${#curr[@]}}
  do
    xrandr --output ${outputs[i]} --brightness 0.3 || echo "Cannot dim '${outputs[i]}' to 0.3"
  done

  sleep $every

  for i in {1..${#curr[@]}}
  do
    xrandr --output ${outputs[i]} --brightness ${curr[i]} || echo "[ERROR] Cannot bright up '${outputs[i]}' to '${curr[i]}'"
  done
done

